import std.file;
import std.path;
import std.process;
import std.array;
import std.algorithm;
import std.conv;
import std.json;
import std.string;
import std.regex;
import std.stdio : writeln;

class ContractBuilder
{
    string contractFolder = "contracts";
    string buildFolder = "build";
    string[] solcCommand = ["solc"];//"docker run ethereum/solc:0.8.9 -v=/:./".split;
    string[] solcArgs = [
        "--optimize", "--base-path=contracts", "--include-path=node_modules"
    ];
    private string buildstring;
    private JSONValue contracts;
    string[string] spaceholders;

    string getBuildString()
    {
        string[] solFiles = dirEntries(contractFolder, "*.sol", SpanMode.depth).map!"a.name".array;
        string[] args = [];
        args ~= solcCommand;
        args ~= solcArgs;
        args ~= [
              "--bin", "--combined-json", "abi,bin"
        ];
        args ~= solFiles;
        auto p = pipeProcess(args, Redirect.stdout);
        return cast(string) p.stdout.byChunk(1024).joiner().array;
    }

    void build(){
        buildstring = getBuildString;
        buildstring.split[0][1].writeln;
        JSONValue buildjson = buildstring.split('\n')[0].parseJSON;
        foreach (key; buildjson[`contracts`].object.keys)
        {
            auto contract = key.split(':')[$ - 1];
            contracts[contract] = buildjson[`contracts`][key];
        }
        parseLibs;
    }

    void generateBindings()
    {
        string stred = spaceholders.to!string;
        auto bindings = buildPath("source/contracts");
        if (!"source/contracts".exists)
            mkdir("source/contracts");

        foreach (key; contracts.object.keys)
        {
            import std.string;

            bindings.buildPath(key ~ ".d").write(q{
module contracts.%s;
import deth;
alias %s = Contract!(ContractABI(`%s`, `%s`));
private alias contract = %s;

shared static  this(){
    import std: canFind, indexOf;
    import core.sync.mutex: Mutex;
    import std: read, getSize, parseJSON;

    string file = "build/%s.json";
    string _bytecode = parseJSON(cast(string) file.read )["bin"].str;
    string[string] _spaceholders %s;
    size_t[string] spaceholders;
    bytes bytecode;
    contract.__bytecodeMutex= new Mutex;
    contract.bytecode_s = _bytecode;
    if(!_bytecode.canFind("_$")){
        contract.bytecode =_bytecode.convTo!bytes;
    }
    else{
        foreach(key; _spaceholders.keys){
            contract.spaceholders[key] = _bytecode.indexOf("__"~_spaceholders[key]~"__");
        }
    }

}
}.format(key, key, contracts[key]["abi"], key, key, key, (stred == "[]" ? "" : "=" ~ stred)));
        }
        import std : join, map;

        bindings.buildPath("package.d")
            .write(
                "module contracts;\n" ~ contracts.object.keys.map!(
                    a => "public import contracts." ~ a ~ ";").join
            );
    }

    void generateArtifacts()
    {
        if (!buildFolder.exists)
            mkdir(buildFolder);
        foreach (string key; contracts.object.keys)
        {
            buildPath(buildFolder, key ~ ".json").write(contracts[key].toPrettyString);
        }
    }

    private void parseLibs()
    {
        auto r = ctRegex!`\/\/ (\$.{34}\$) ->.*:(.*)`;
        foreach (m; matchAll(buildstring, r))
        {
            spaceholders[m[2]] = m[1];
        }
    }

}

void main()
{
    import std;

    auto b = new ContractBuilder;
    b.build;
    b.generateArtifacts;
    b.generateBindings;
}
