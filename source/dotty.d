module dotty;

public import deth;

mixin template ContractTestContructor(){
	this(string rpc){
		super(rpc);
	}
}

class ContractTest
{
	RPCConnector conn;

	public this(string rpc)
	{
		this.conn = new RPCConnector(rpc);
	}

	void before()
	{
	}

	void beforeEach()
	{
	}

	void afterEach()
	{
	}

	void after()
	{
	}
}
