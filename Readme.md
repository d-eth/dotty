# Dotty
## Smart-Contract Test runner

### Creating project with test runner
```sh
dub init -t dotty <your project  folder>
```

```d
import dotty;
import contracts: Registry;

class ExampleTest: ContractTest
{
    /// test variables
    Registry registry;
    /// contructor for ContractTest neccesery for any Test
    mixin ContractTestContructor;
    /// function running before all tests once
    override void before(){
        registry = Registry.deploy(conn, conn.remoteAccounts[1]);
    }

    @("registry should setable")
    void test()
    {
        registry.set("registry", registry.address).send(conn.remoteAccounts[1].From);
        assert(registry.get("registry") == registry.address, "addr must be in registry");
    }
}
```