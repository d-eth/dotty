module app;

import std.path : buildPath;
import std.file : getcwd, mkdir, write, getSize, read, exists;
import std.string : replace;
import std.json : parseJSON;

string exampletestD = import("exampletest.d");
string configD = import("dotty.json");
string contractD = import("Registry.sol");

void main(string[] args)
{
	string sourceFolder = buildPath(getcwd(), "source");
	mkdir(sourceFolder);

	string exampletestDPath = buildPath(sourceFolder, "registrytest.d");
	write(exampletestDPath, exampletestD);

	string configDPath = buildPath(getcwd, "dotty.json");
	write(configDPath, configD);

	string contractsFolder = buildPath(getcwd, "contracts");
	mkdir(contractsFolder);
	contractsFolder.buildPath("Registry.sol").write(contractD);

	string dubfile;

	string json = buildPath(getcwd, `dub.json`);
	string sdl = buildPath(getcwd, `dub.sdl`);
	if (json.exists)
		dubfile = json;
	if (sdl.exists)
		dubfile = sdl;
	string text = (cast(string) read(dubfile, dubfile.getSize))
		.replace(`"dotty"`, `"dotty:runner"`);

	if (json.exists)
	{
		auto o = text.parseJSON;
		o["preGenerateCommands"] = ["dub run dotty:builder"];
		text = o.toPrettyString;
	}
	if (sdl.exists) 
	{
		text ~= `preGenerateCommands "dub run dotty:builder"`;
	}
	dubfile.write = text;
}
