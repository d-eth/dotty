import dotty;
import contracts: Registry;

class ExampleTest: ContractTest
{
    Registry registry;
    mixin ContractTestContructor;
    override void before(){
        registry = Registry.deploy(conn, conn.remoteAccounts[1]);
    }
    @("registry should setable")
    void test()
    {
        registry.set("registry", registry.address).send(conn.remoteAccounts[1].From);
        assert(registry.get("registry") == registry.address, "addr must be in registry");
    }
}
