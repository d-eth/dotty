contract Registry {
    address public owner;
    constructor(address _owner){
        owner = _owner;
    }
    mapping(bytes32 => address) addresses;

    function get(string memory name) public view returns (address) {
        return addresses[keccak256(abi.encode(name))];
    }

    function set(string memory name, address a) public {
        require(msg.sender == owner);
        addresses[keccak256(abi.encode(name))] = a;
    }
}
